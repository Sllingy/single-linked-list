//
// Created by tomkr on 25/06/2019.
//

#ifndef SINGLELINKEDLIST_NODE_H
#define SINGLELINKEDLIST_NODE_H

#include <memory>

template <typename T>
struct Node {
public:
    explicit Node(T data);
    std::shared_ptr<Node<T>> next;
    T data;
};

template <typename T>
Node<T>::Node(T data) : data(data), next(nullptr) {}

#endif //SINGLELINKEDLIST_NODE_H
