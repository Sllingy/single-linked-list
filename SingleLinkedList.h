//
// Created by tomkr on 26/06/2019.
//

#ifndef SINGLELINKEDLIST_SINGLELINKEDLIST_H
#define SINGLELINKEDLIST_SINGLELINKEDLIST_H

#include <memory>
#include <iostream>
#include "Node.h"

template <typename T>
using node_ptr = std::shared_ptr<Node<T>>;

template <typename T>
class SingleLinkedList {
private:
    node_ptr<T> head;
    size_t size;
public:
    SingleLinkedList();
    void PushBack(T data);
    void PushFront(T data);
    bool Empty();
    node_ptr<T> Front();
    node_ptr<T> Back();
    int Size();
    void InsertAt(T data, size_t index);
    void EraseAt(size_t index);
    node_ptr<T> GetAt(size_t index);
    void Display();
};

template <typename T>
SingleLinkedList<T>::SingleLinkedList() : size(0), head(nullptr) {}

template <typename T>
void SingleLinkedList<T>::PushBack(T data) {
    auto node = node_ptr<T>(new Node<T>(data));

    if(Empty()) {
        head = node;
    } else {
        auto current = head;
        for (int i = 0; i < size - 1; ++i) {
            current = current->next;
        }
        current->next = node;
    }
    size++;
}

template <typename T>
void SingleLinkedList<T>::PushFront(T data){
    auto node = node_ptr<T>(new Node<T>(data));

    if(Empty()) {
        head = node;
    } else {
        node->next = head;
        head = node;
    }
    size++;
}

template <typename T>
bool SingleLinkedList<T>::Empty() {
    return size == 0;
}

template <typename T>
node_ptr<T> SingleLinkedList<T>::Front() {
    return head;
}

template <typename T>
node_ptr<T> SingleLinkedList<T>::Back(){
    auto current = head;
    for (int i = 0; i < size - 1; ++i) {
        current = current->next;
    }
    return current;
}

template <typename T>
int SingleLinkedList<T>::Size(){
    return size;
}

template <typename T>
void SingleLinkedList<T>::InsertAt(T data, size_t index){
    if (index > size - 1) {
        throw std::out_of_range("Index out of range!");
    }

    auto node = node_ptr<T>(new Node<T>(data));
    if(index == 0) {
        PushFront(data);
        return;
    }
    auto current = head;
    for (int i = 0; i < index - 1; ++i) {
        current = current->next;
    }
    node->next = current->next;
    current->next = node;
    size++;
}

template <typename T>
void SingleLinkedList<T>::EraseAt(size_t index){
    if (index > size - 1) {
        throw std::out_of_range("Index out of range!");
    }

    auto current = head;
    if(index == 0)
        head = head->next;
    else {
        for (int i = 0; i < index - 1; ++i) {
            current = current->next;
        }
        current->next = current->next->next;
    }
    size--;
}

template <typename T>
node_ptr<T> SingleLinkedList<T>::GetAt(size_t index){
    if (index > size - 1) {
        throw std::out_of_range("Index out of range!");
    }

    auto current = head;

    if(index == 0)
        return head;
    else {
        for (int i = 0; i < index; ++i) {
            current = current->next;
        }
        return current;
    }
}

template <typename T>
void SingleLinkedList<T>::Display() {
    auto node = head;
    for (int i = 0; i < size; ++i) {
        std::cout << node->data << "\t";
        node = node->next;
    }
    std::cout << '\n';
}

#endif //SINGLELINKEDLIST_SINGLELINKEDLIST_H
