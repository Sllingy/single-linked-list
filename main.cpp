#include <cstdio>
#include <iostream>
#include "SingleLinkedList.h"

int main() {
    SingleLinkedList<int> list;

    for (int i = 5; i > 0; --i) {
        list.PushBack(i - 1);
    }
    list.Display();

    for (int i = 5; i < 10; ++i) {
        list.PushFront(i);
    }
    list.Display();

    std::cout << "Front: " << list.Front()->data
    << "\tBack: " << list.Back()->data << std::endl;

    std::cout << "Size:" << list.Size() << std::endl;

    list.InsertAt(88, 5);
    list.Display();

    std::cout << list.GetAt(10)->data;
    return 0;
}